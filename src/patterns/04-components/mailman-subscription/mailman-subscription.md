---
el: .uw-mailman
title: Mailman
---

__Variables:__
* mailman: [array] Variables for mailman.
    * server: [string] Mailman server url.
    * servername: [string] Mailman server name.
    * uniqueid: [string] Unique id of the mailman component.
