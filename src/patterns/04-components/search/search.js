/**
 *  Javascript enhancements for the header search.
 *
 * @file
 */

(function ($, document, Drupal) {
  'use strict';
  Drupal.behaviors.wcmsheadersearchbar = {
    attach: function () {
      $(document).ready(
        function () {
          $('.uw-search--checkboxlabel').on(
            'click', function () {
              $(this).toggleClass('close');
              $('.uw-header__masthead').toggleClass('open');
              $('.uw-header-search').css('z-index', '12');
            }
          );

          if ($('.uw-header__masthead').hasClass('open') || $('#edit-opentray').is(':checked')) {
            $('.uw-header-search').css('z-index', '12');
            $('.uw-header__masthead').toggleClass('open');
          }
          /**
           * Javascript for debounce
           * @param {func} func The function to perform
           * @returns {function} debounce.
           */
          function debouncer(func) {
            var timeoutID;
            var timeout = 100;
            return function () {
              var scope = this;
              var args = arguments;
              clearTimeout(timeoutID);
              timeoutID = setTimeout(
                function () {
                  func.apply(scope, Array.prototype.slice.call(args));
                }, timeout
              );
            };
          }

          /**
           * Function to check the width.
           * @returns {null} Null return.
           */
          function checkWidth() {
            // Set screenWidth var.
            var screenWidth = $(window).width();
            if (screenWidth >= 768) {
              if ($('.uw-header__masthead').hasClass('open')) {
                $('.uw-search--checkboxlabel').click();
              }
            }
          }
          // Listen to event resize and apply the debouncer
          // to the menuCheckWidth function.
          $(window).resize(
            debouncer(
              function () {
                checkWidth();
              }
            )
          );
          checkWidth();
        }
      );
    }
  };
})(jQuery, document, Drupal);
