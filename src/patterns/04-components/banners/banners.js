/**
 * @file
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.banners = {
    attach: function () {
      $(document).ready(function () {

        // Step through each Banners on the page.
        $('.uw-carousel__banner').each(function () {

          var selector = 'div[data-uuid="' + $(this).attr('data-uuid') + '"] .owl-carousel';

          // Get  banner carousel.
          var owl = $(selector);

          // Get the number of items for the carousel, if any.
          // For banners we are only ever showing one item at
          // a time (for reference Facts & Figures show more).
          var numOfItems = 1;

          var bannerSlideSpeed = $(this).attr('data-slide-speed');

          // The flag for autoplay.
          var bannerAutoplay = true;

          // If there is no autoplay, or we are in layout builder,
          // set flag to false.
          if ($(this).attr('data-autoplay') === 0 || $('.layout-builder').length) {
            bannerAutoplay = false;
          }

          // Used to hide buttons when single banner
          if (owl.children().length <= 1) {
            $(this).addClass('banner-single');
          }

          // Get the loop value.
          // Owl carousel setting to loop back to beginning.
          var bannerLoop = false;
          var bannerRewind = true;

          // Get dots.
          // Owl carousel setting show dots.
          // If banner has more than 1 slide show dots.
          var bannerDots = false;
          if (owl.children().length > 1) {
            bannerDots = true;
          }
          // Get nav.
          // Owl carousel setting show prev next.
          var bannerNav = false;
          if ($(this).hasClass('uw-carousel__banner-inset')) {
            bannerNav = true;
          }

          // Get the play and stop buttons.
          var bannerPlay = $(this).find($('.uw-play'));
          var bannerPause = $(this).find($('.uw-pause'));

          // Actions for play button.
          bannerPlay.on('click',function () {
            owl.trigger('play.owl.autoplay', [bannerSlideSpeed]);
            bannerPlay.css('display', 'none');
            bannerPause.css('display', 'block');
          });

          // Actions for pause button.
          bannerPause.on('click',function () {
            owl.trigger('stop.owl.autoplay');
            bannerPause.css('display', 'none');
            bannerPlay.css('display', 'block');
          });

          // Add the carousel to the banner using the id.
          owl.owlCarousel({
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            autoplay: bannerAutoplay,
            autoHeight : true,
            autoplayTimeout: bannerSlideSpeed,
            autoplayHoverPause: true,
            dots: bannerDots,
            dotsClass: 'uw-owl-nav__dots',
            loop: bannerLoop,
            rewind: bannerRewind,
            nav: bannerNav,
            navContainerClass: 'uw-owl-nav__prevnext',
            navText:[
              '',
              ''
            ],
            responsiveClass: true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: numOfItems <= 2 ?
                  (numOfItems - 1 > 0) ?
                    numOfItems - 1 : 1 : 2
              },
              1000: {
                items: numOfItems
              }
            },
          });
        }).on('click', '.owl-dot', function () {
          // Pause the slideshow if they click to advance
          // to a specific slide. We don't need to check
          // if this is present because jQuery will just
          // ignore this if it isn't.
          $(this).closest('.uw-carousel__banner').find('.uw-pause').click();
        });
      });
    }
  };
})(jQuery, Drupal);
