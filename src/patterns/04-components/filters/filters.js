/**
 * @file
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.filtersopen = {
    attach: function () {
      $(document).ready(function () {
        $('.view-filters details').first().attr('open', '');
      });
    }
  };
})(jQuery, Drupal);
