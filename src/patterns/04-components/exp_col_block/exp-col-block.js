/**
 * @file
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.expcol = {
    attach: function () {
      $(document).ready(function () {

        $('.uw-exp-col').each(function () {

          // The id selector for the exp/col.
          var idSelector = '#' + $(this).attr('id');

          // Open all the details for the clicked E/C.
          $(idSelector + ' button[data-type="expand-all"]').click(function () {
            $(idSelector + ' details').each(function () {
              $(this).attr('open', 'TRUE');
            });
          });

          // Close all the details for the clicked E/C.
          $(idSelector + ' button[data-type="collapse-all"]').click(function () {
            $(idSelector + ' details').each(function () {
              $(this).removeAttr('open');
            });
          });
        });
      });
    }
  };
})(jQuery, Drupal);
