---
el: .uw-social-intents
title: Social Intents
---

__Variables:__
* social-intents: [array] Variables for social-intents.
    * username: [string] Social Intents username.
    * uniqueid: [string] Unique id of the social-intents component.
