---
el: .tag
title: Tag
---

__Variables:__
* type: [string] Type of the tag
    * full
    * simple
* size: [string] Size of the tag.
    * small
    * normal
* url: [string] URL of the tag.
* title: [string] Title of the tag.
* faculty: [string] One of the faculty variables wrapping the tag will make
  background color change
    * org-ahs
    * org-art
    * org-eng
    * org-env
    * org-mat
    * org-sci
    * org-school
