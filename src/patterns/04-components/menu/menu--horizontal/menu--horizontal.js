/**
 * @file
 */

(function ($, document, Drupal) {
  'use strict';
  Drupal.behaviors.menuhorizontal = {
    attach: function (context) {
      // uw-horizontal-nav.
      $(document).ready(
        function () {

          // Have to add the run this code only once, so that multiple
          // loads of the menu are not shown when logged in.
          $(document, context).once('menuhorizontal').each(
            function () {

              var toggle = document.querySelector('.uw-navigation-button');
              var navHeader = document.querySelector('.uw-header__navigation');
              var menus = document.querySelectorAll('.menu--horizontal');
              var items = document.querySelectorAll('.menu__item');
              var globalMenu = document.querySelector('.uw-header-menu');
              var globalSub = document.querySelector('.menu').querySelector('.menu');

              /**
               * Javascript for Toggle mobile menu,
               * allows for buttons to be used.
               * @returns {boolean} css for toggle.
               */
              function toggleMenu()              {

                if (this.classList.contains('active')) {
                  this.classList.remove('active');
                  this.setAttribute('aria-expanded', 'false');
                  navHeader.classList.remove('open');
                  navHeader.classList.add('close');
                  $('html').removeClass('no-scroll');
                }
                else {
                  this.classList.add('active');
                  this.setAttribute('aria-expanded', 'true');
                  navHeader.classList.remove('close');
                  navHeader.classList.add('open');
                  $('html').addClass('no-scroll');

                }

              }

              /**
               * Javascript for Toggle items,
               * allows for buttons to be used.
               * @returns {boolean} css for toggle.
               */
              function toggleItem() {

                var parent = this.parentNode.parentNode.parentNode.parentNode;

                var screenWidth = $(window).width();

                if (this.classList.contains('submenu-active')) {

                  this.classList.remove('submenu-active');

                  if (this.hasAttribute('aria-expanded', 'true')) {

                    this.setAttribute('aria-expanded', 'false');
                  }

                  // If hamburger.
                  if (screenWidth <= 767) {
                    // Look at parents and reset the menus.
                    if (parent.classList.contains('uw-horizontal-nav--secondary')) {
                      $('.uw-horizontal-nav--main').css('display', 'block');
                    }
                  }
                }
                else if ($('.submenu-active')) {

                  // Get elements with .submnenu-active then close them,.
                  $('.submenu-active').removeClass('submenu-active').attr('aria-expanded', 'false');

                  // And open this one.
                  this.classList.add('submenu-active');
                  this.setAttribute('aria-expanded', 'true');

                  // If hamburger.
                  if (screenWidth <= 767) {
                    // Look at parents and hide other menus
                    // if not secondary and open.
                    if (parent.classList.contains('uw-horizontal-nav--secondary')) {
                      $('.uw-horizontal-nav--main').css('display', 'none');
                      $('.uw-horizontal-nav--secondary').css('display', 'block');
                    }
                  }
                }
                else {
                  this.classList.add('submenu-active');
                  this.setAttribute('aria-expanded', 'true');
                }
              }

              for (let menu of menus) {
                document.addEventListener('click', function (e) {
                  let isClickInside = menu.contains(e.target);

                  if (!isClickInside && menu.querySelector('.submenu-active')) {

                    menu.querySelector('.submenu-active').classList.remove('submenu-active');
                  }
                });
              }

              for (let item of items) {

                if (item.querySelector('.menu__subnav')) {
                  item.addEventListener('click', toggleItem, false);
                }

                // Add event listeners to keyup event of enter
                // and escape keys for the menu--items .
                item.addEventListener(
                  'keyup', function (e) {

                    if (e.key === 'Enter') {
                      this.click();
                    }
                    if (e.key === 'Escape') {
                      this.click();
                    }

                    // Space bar keypress to open close menu
                    // keyCode to be deprecated find way to use key.
                  }
                );
                item.addEventListener(
                  'keypress', function (e) {
                    if (e.keyCode === 32) {
                      this.click();
                      e.preventDefault();
                    }
                  }
                );
              }

              // If Toggle on page Add event
              // listeners on the menu toggle button.
              if (toggle) {
                toggle.addEventListener('click', toggleMenu, false);
              }

              /**
               * Javascript for debounce
               * @param {func} func, the type of function.
               * @returns {function} debounce.
               */
              function debouncer(func) {
                var timeoutID;
                var timeout = 0;
                return function () {
                  var scope = this;
                  var args = arguments;
                  clearTimeout(timeoutID);
                  timeoutID = setTimeout(
                    function () {
                      func.apply(scope, Array.prototype.slice.call(args));
                    }, timeout
                  );
                };
              }

              // Check the width of the screen and
              // force the button click if wider that 767px.
              /**
               * Javascript for check width
               * @returns {function} how wide.
               */
              function menuCheckWidth() {
                // Set screenWidth var.

                var screenWidth = $(window).width();

                if (globalMenu.classList.contains('uw-header-menu__full')) {

                  if (screenWidth > 1010) {
                    globalSub.classList.remove('menu__subnav');
                    globalSub.classList.add('menu__subnav--global');
                  }
                  else {
                    globalSub.classList.add('menu__subnav');
                    globalSub.classList.remove('menu__subnav--global');
                  }
                }

                // Check if menu is on page.
                if (navHeader) {

                  if (screenWidth > 767) {
                    if ($('html').hasClass('no-scroll')) {
                      toggle.click();
                      $('.uw-horizontal-nav').css('display', 'block');
                    }
                    else {
                      $('.uw-header__navigation').addClass('close');
                    }
                  }
                  else {
                    if ($('.uw-header__navigation').hasClass('open')) {
                      $('.uw-header__navigation').removeClass('open');
                      $('.uw-header__navigation').addClass('close');
                    }
                  }
                }
              }

              // Listen to event resize and apply the debouncer
              // to the menuCheckWidth function.
              $(window).resize(
                debouncer(
                  function () {
                    menuCheckWidth();
                  }
                )
              );
              menuCheckWidth();
            }
          );
        }
      );
    }
  };
})(jQuery, document, Drupal);
