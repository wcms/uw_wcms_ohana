---
el: .uw-ff
title: Facts and Figures
---

__Variables:__
* ffs: [array] Classes to modify the default component styling.
    * id: [string] Unique id of the fact and figure.
    * show: [boolean] Show the bubbles on the fact and figure.
    * theme: [string] The theme/faculty to be applied to the fact and figure.
    * text_align: [string] Which way the text is to be aligned.
    * details: [array] Details about the text of the fact and figure.
        * icon/text: [string] Either the string of text or the URL of the icon.
        * type: [string] The type of text (big, medium, small or icon).
