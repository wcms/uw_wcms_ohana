/**
 * @file
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.cardbanner = {
    attach: function () {
      $(document).ready(function () {
        // Clicking play/pause button on video.
        $('.uw-video-wrapper').each(function () {
          var id = $(this).attr('id');
          var btn = '#btn-' + $(this).attr('id');
          var videoTarget = $('#video-' + id);
          var bannerBtn = $(btn);

          bannerBtn.click(function () {
            var video = videoTarget.get(0);
            if (video.paused) {
              video.play();
              $(this).removeClass('uw-video-play');
              $(this).addClass('uw-video-pause');
              $(this).attr('aria-label', 'Pause');
              $(this).attr('title', 'Pause video autoplay');
            }
            else {
              video.pause();
              $(this).removeClass('uw-video-pause');
              $(this).addClass('uw-video-play');
              $(this).attr('aria-label', 'Play');
              $(this).attr('title', 'Resume video autoplay');
            }
          });
        });
      });
    }
  };
})(jQuery, Drupal);
