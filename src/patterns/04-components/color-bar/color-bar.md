---
el: .colour-bar
title: Colour bar
---

__Variables:__
* faculty: [string] The faculty of the colour bar (chooses the colour)
