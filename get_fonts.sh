echo "*************************************************************************"
echo "Getting fonts ..."
if [[ -d ../../../../../config/fonts ]]; then
  if [[ -d dist/fonts ]]; then
    rm -rf dist/fonts/*
  fi
  cp ../../../../../config/fonts/* dist/fonts
else
  echo "Error: there are no fonts installed, run ./setup_site.sh"
fi
echo "Done getting fonts ..."
echo "*************************************************************************"
