'use strict';

// Include gulp
const { src, dest } = require('gulp');

// Include Our Plugins
const sass = require('gulp-sass')(require('sass'));
const prefix = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const minify = require('gulp-minify');
const clean = require('gulp-clean-css');

/**
 * Error handler function so we can see when errors happen.
 * @param {object} err error that was thrown
 * @returns {undefined}
 */
function handleError(err) {
  // eslint-disable-next-line no-console
  console.error(err.toString());
  this.emit('end');
}

// Export our tasks.
module.exports = {

  // Compile Sass.
  compileDashboard: function () {
    return src([
        './src/patterns/dashboard.scss'
      ])
      .pipe(sass.sync({
        includePaths: ['node_modules'],
        outputStyle: 'expanded'
      }).on('error', handleError))
      .pipe(prefix({ cascade: false }))
      .pipe(rename(function (path) { path.dirname = ''; return path;}))
      .pipe(clean())
      .pipe(rename('uw-dashboard.min.css'))
      .pipe(dest('./dist/css'));
  }
};
