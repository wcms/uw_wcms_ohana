env_option=${1:-2}
build_option=${2:-1}
PL_PATH=${3:-"/var/www/html/web"}
DRUPAL_PATH=${4:-"/var/www/html"}
os_option=${5:-1}

if [[ -L "$PL_PATH/uw_wcms_ohana" || -d "$PL_PATH/uw_wcms_ohana" ]]; then
  echo ""
  echo "*************************************************************************"
  echo "Removing old ohana symlink ..."
  rm -rf "$PL_PATH/uw_wcms_ohana"
  echo "Done removing old ohana symlink."
  echo "**************************************************************************"
  echo ""
fi

cd "$DRUPAL_PATH/web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana"

if [[ -d ohana ]]; then
  echo "**************************************************************************************"
  echo "Removing old ohana ..."
  rm -rf ohana
  echo "Done removing old ohana."
  echo "**************************************************************************************"
  echo ""
fi

git clone --branch patternlab https://git.uwaterloo.ca/wcms/kpaxman-ohana.git ohana

if [ "$?" != 0 ]; then
  echo "Error: git clone failed"
  exit 2;
fi

cd ohana

echo "**************************************************************************************"
echo "Installing correct npm version ..."
file=".nvmrc"
version=$(cat "$file")
export NVM_DIR=$HOME/.nvm;
source $NVM_DIR/nvm.sh;
nvm install $version
echo "**************************************************************************************"
echo "Done installing correct npm version."

echo "**************************************************************************************"
echo "Setting up theming ..."
cd ..
if [[ -d dist ]]; then
  rm -rf dist
fi
if [[ $build_option == 1 || $build_option == 3 ]]; then
  ln -s ohana/dist dist
fi
cd ohana
rm -rf src/patterns
ln -s ../../src/patterns src/patterns
echo "**************************************************************************************"
echo "Done setting up theming."

if [[ -d "$DRUPAL_PATH"/web/config/fonts ]]; then
  echo "**************************************************************************************"
  echo "Copying fonts ..."
  cp "$DRUPAL_PATH"/web/config/fonts/* dist/fonts
  echo "**************************************************************************************"
  echo "Done copying fonts."
fi

echo "**************************************************************************************"
echo "Running npm install ..."
if [[ $os_option == 1 ]]; then
  export CPPFLAGS="-DPNG_ARM_NEON_OPT=0"
fi
cp ../php8fix-package_run1.json ./package.json
npm install
cp ../php8fix-package.json ./package.json
npm install
sed -i '/foreach ($array as $k => $v)/iif (empty($array)) return $r;' node_modules/@basalt/twig-renderer/vendor/twig/twig/src/Extension/CoreExtension.php
echo "**************************************************************************************"
echo "Done running npm install."

echo "**************************************************************************************"
echo "Setting up es lint and gulp ..."
rm .eslintignore
ln -s ../.eslintignore .eslintignore
ln -s ../.stylelintrc.json .stylelintrc.json
cp ../dashboard.js gulp-tasks
sed -i "3 i const { compileDashboard } = require('./gulp-tasks/dashboard');" gulpfile.js
sed -i "s/parallel(compileSass, compileJS)/parallel(compileSass, compileJS, compileDashboard)/g" gulpfile.js
cd gulp-tasks
sed -i "17 i '!./dist/css/*dashboard*.css'," concat.js
cd ..
echo "**************************************************************************************"
echo "Done setting up es lint."

echo "**************************************************************************************"
echo "Running npm run build ..."
npx browserslist@latest --update-db
npm run build
echo "**************************************************************************************"
echo "Done running npm run build."

echo "**************************************************************************************"
echo "Setting up pattern-lab ..."
cd patternlab
mkdir core
cd core
ln -s "$DRUPAL_PATH"/web/core/assets assets
ln -s "$DRUPAL_PATH"/web/core/misc misc
cd ..
cd ..
cd src/styleguide/meta
rm _foot.twig
ln -s ../../../../styleguide/_foot.twig _foot.twig
cd ../../..
echo "**************************************************************************************"
echo "Done setting up pattern-lab."

if [[ $build_option == 2 || $build_option == 4 ]]; then
  echo "**************************************************************************************"
  echo "Setting up css/js and removing ohana ..."
  cd ..
  mv ohana/dist .
  cd dist/css
  find . -type f -not \( -name 'all.min.css' -or -name 'uw-dashboard.min.css' \) -delete
  cd ../js
  find . -type f -not -name 'all.min.js' -delete
  cd ..
  if [[ -d vendor ]]; then
    rm -rf vendor
  fi
  cd "$DRUPAL_PATH"/web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana/
  rm -rf ohana
  echo "**************************************************************************************"
  echo "Done setting up css/js and removing ohana."
fi

if [[ $env_option == 1 ]]; then
  echo "**************************************************************************************"
  echo "Creating symlink for ohana ..."
  ln -s "$DRUPAL_PATH"/web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana "$PL_PATH"/uw_wcms_ohana
  echo "Done creating symlink for ohana."
  echo "**************************************************************************************"
  echo ""
fi
